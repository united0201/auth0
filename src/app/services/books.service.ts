import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Books } from '../dto/books.model';

@Injectable( {
  providedIn: 'root'
} )
export class BooksService {
  constructor( private http: HttpClient, private auth: AuthService ) {
  }

  private authHeader(): object {
    return {
      headers: new HttpHeaders( {'Authorization': `Bearer ${ this.auth.getAccessToken() }`} )
    };
  }

  public getBooks(): Observable<Books[]> {
    return this.http.get<Books[]>( 'http://10.1.0.60:3000/books/' );
  }

  public postBook( book: Books ): Observable<Books> {
    return this.http.post<Books>( 'http://10.1.0.60:3000/books', book, this.authHeader() );
  }
}
