import { Injectable } from '@angular/core';
import { AuthOptions, WebAuth } from 'auth0-js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable( {
  providedIn: 'root'
} )
export class AuthService {
  protected auth0Client: WebAuth;
  private accessToken: string;
  private idToken: string;
  private readonly properties: AuthOptions;
  private expires = 86400;
  private expiresAt: string;

  constructor( private http: HttpClient, private router: Router ) {
    this.properties = {
      clientID: 'vEfDET13Fuwl4hpYal1owA7TjBttY8ia',
      domain: 'dev-n94f6tle.eu.auth0.com',
      responseType: 'token id_token',
      audience: 'http://localhost:3000',
      redirectUri: 'http://10.1.1.83:4200',
      scope: 'openid profile'
    };

    this.auth0Client = new WebAuth( {...this.properties} );

    const token = window.localStorage.getItem( 'accessToken' );
    if ( token ) {
      const result = {
        accessToken: token,
        idToken: token
      };
      this._setSession( result );
    }
  }

  public loginUser(): void {
    this.auth0Client.authorize( {
      redirectUri: 'http://10.1.1.83:4200',
      responseType: 'code',
    }, ( err, res ) => {
      console.log( err );
      console.log( res );
    } );
  }

  public checkSession(): Promise<boolean> {
    return new Promise<boolean>( ( resolve, reject ) => {

      this.auth0Client.checkSession( this.properties, async ( error, authResult ) => {
        const codePrw = this.router.url.split( '=' );
        if ( codePrw.length > 1 ) {
          const code = codePrw[1].substring( 0, codePrw[1].indexOf( '&' ) );
          if ( !authResult && code && code.length > 0 ) {
            this.http.get<any>( `http://10.1.0.60:3000/auth/${ code }`,
              {headers: new HttpHeaders( {'Content-Type': 'application/json'} )} )
              .subscribe( tokens => {
                this.expires = tokens.expires_in;
                const result = {
                  accessToken: tokens.access_token,
                  idToken: tokens.access_token
                };
                this._setSession( result );
              } );
          }
        }

        this.expiresAt = JSON.stringify( (this.expires * 1000) + new Date().getTime() );

        if ( error && error.error !== 'login_required' ) {
          return reject( error );
        } else if ( error ) {
          this.handleAuthentication();
          return resolve( false );
        }
        if ( !this.isAuthenticated() ) {
          this._setSession( authResult );
          return resolve( true );
        }
      } );
    } );
  }

  public isAuthenticated(): boolean {
    return this.accessToken != null && (new Date().getTime() < Number.parseFloat( this.expiresAt ));
  }

  private handleAuthentication(): void {
    this.auth0Client.parseHash( ( err, authResult ) => {
      if ( authResult && authResult.accessToken && authResult.idToken ) {
        window.location.hash = '';
        this._setSession( authResult );
      } else if ( err ) {
        console.log( err );
      }
    } );
  }

  private _setSession( authResult ): void {
    this.accessToken = authResult.accessToken;
    this.idToken = authResult.idToken;
    window.localStorage.setItem( 'accessToken', this.accessToken );
    this.router.navigate( ['/'] );
  }

  public isAdmin(): boolean {
    if ( this.accessToken ) {
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken( this.accessToken );
      console.log( decodedToken );
      return decodedToken['http://localhost:3000/roles'].indexOf( 'admin' ) > -1;
    } else {
      return false;
    }
  }

  public getAccessToken(): string {
    return this.accessToken;
  }

  public logout(): void {
    delete this.accessToken;
    delete this.idToken;
    this.auth0Client.logout( {
      returnTo: 'http://10.1.1.83:4200',
      clientID: 'vEfDET13Fuwl4hpYal1owA7TjBttY8ia'
    } );
    window.localStorage.removeItem( 'accessToken' );
  }
}


