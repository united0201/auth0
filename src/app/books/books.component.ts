import { Component, OnInit } from '@angular/core';
import { Books } from '../dto/books.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { BooksService } from '../services/books.service';

@Component( {
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.sass']
} )
export class BooksComponent implements OnInit {
  books: Books[] = [];

  addBookForm: FormGroup;
  bookSubmitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private bookService: BooksService ) {
  }

  ngOnInit() {
    this.bookService.getBooks().subscribe( books => {
      this.books = books;
    } );

    this.addBookForm = this.formBuilder.group( {
      title: ['', Validators.required],
      price: ['', [Validators.required, Validators.min( 0 )]],
      link: ['', Validators.required]
    } );
  }

  get getBookForm() {
    return this.addBookForm.controls;
  }

  addNewBook() {
    this.bookSubmitted = true;
    if ( this.addBookForm.invalid ) {
      console.log( this.addBookForm );
    } else {
      this.books.push( this.addBookForm.value as Books);
      this.bookService.postBook(this.addBookForm.value as Books).subscribe(value => console.log('res', value));
      console.log( this.addBookForm.value );
      this.addBookForm.reset();
    }
  }

  isAdmin() {
    return this.authService.isAdmin();
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

}
