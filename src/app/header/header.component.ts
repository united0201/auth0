import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component( {
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
} )
export class HeaderComponent implements OnInit {
  title = 'Book store';

  constructor( private authService: AuthService ) {
  }

  ngOnInit() {
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  loginUser() {
    return this.authService.loginUser();
  }

  logout() {
    return this.authService.logout();
  }
}
